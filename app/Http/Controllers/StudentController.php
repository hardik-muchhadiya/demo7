<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;

class StudentController extends Controller
{
    public function index(){
        return view('student.index');
    }
    public function create(){
        return view('student.create');
    }
    public function Store(Request $request){
        // dd($request);
        $student = new Student();
        $student->name=$request->input('name');
        $student->email=$request->input('email');
        $student->course=$request->input('course');
        if($request->hasfile('profile_image'))
        {
            $file = $request->file('profile_image');
            $extention= $file->getClientOriginalExtension();
            $filename = time(). '.'.$extention;
            $file->move('uploads/students/',$filename);
            $student->profile_image = $filename;
        }
        $student->save();
        // $student->profile_image = $request->input('profile_image');

        // return redirect('student.imageView')->back()->with('status','Student Image successfully Added');
        return view('student.imageView',compact('filename'));

        

    }
}
