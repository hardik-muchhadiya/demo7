<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
class FileUploadController extends Controller
{
    public function fileUpload()
    {
        return view('fileUpload');
    }
    public function fileUploadPost(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:pdf,xlx,csv|max:2048',
        ]);
        // dd($request);
        $file = $request->file;
        $path ='user/';
        $fileName   = time() . $file->getClientOriginalName();
        Storage::disk('public')->put($path . $fileName, File::get($file));
        $file_name  = $file->getClientOriginalName();
        $file_type  = $file->getClientOriginalExtension();
        $filePath   = 'storage/'.$path . $fileName;
        $fileName = time().'.'.$file->extension();  
        // dd($fileName);
        $request->file->move(public_path('uploads'), $fileName);
   
        return back()
            ->with('success','You have successfully upload file.')
            ->with('file',$fileName);
   
    }
}
