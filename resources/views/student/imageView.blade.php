<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>uploaded image</h4>
                    </div>
                    <div class="card-body">
                        <img src="{{url('uploads/students/'.$filename)}}" alt="error in image" srcset=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>